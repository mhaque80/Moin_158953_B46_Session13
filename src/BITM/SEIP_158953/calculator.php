<?php

namespace App;


class Calculator
{
    private $number1;
    private $number2;
    private $operation;
    private $result;

    public function setNumber1($number1)
    {
        $this->number1 = $number1;
    }

    public function setNumber2($number2)
    {
        $this->number2 = $number2;
    }

    public function setOperation($operation)
    {
        $this->operation = $operation;
    }

    public function setResult()
    {
        switch($this->operation){
            case "Addition":
                $this->result=$this->number1 + $this->number2;
                break;
            case "Substruction":
                $this->result=$this->number1 - $this->number2;
                break;
            case "Multiplication":
                $this->result=$this->number1 * $this->number2;
                break;
            case "Division":
                $this->result=$this->number1 / $this->number2;
                break;
            }
    }

    public function getNumber1()
    {
        return $this->number1;
    }

    public function getNumber2()
    {
        return $this->number2;
    }

    public function getOperation()
    {
        return $this->operation;
    }

    public function getResult()
    {
        $this->result;
        return $this->result;
    }
}